﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private Vector3 _startPosition;
    private Vector3 _endPosition;
    private readonly float _flySpeed = 1f;
    private float _timeFlown = 0f;

    private readonly Vector3 _offset = new Vector3(5, 0);

    void Start()
    {
        _startPosition = transform.position;
        _endPosition = _startPosition + _offset;
    }

    void Update()
    {
        _timeFlown += Time.deltaTime / _flySpeed;
        transform.position = Vector3.Lerp(_startPosition, _endPosition, _timeFlown);

        if (transform.position.x >= _endPosition.x)
        {
            Destroy(gameObject);
        }
    }
}
