﻿using UnityEngine;

public class WinScreen : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        var musicPlayer = FindObjectOfType<BackgroundMusic>();

        if (musicPlayer != null)
        {
            musicPlayer.StopMusic();
        }

        PlayerHealth.CurrentHealth = 0;
    }
}
