using System.Collections.Generic;

public static class ConversationText
{
    //  <color=#ff0000ff>
    //  </color>
    public const string DatePreconvo1Player = "I'm terrible with words... if only I wasn't such a silent protagonist!";

    public const string DatePreconvo2Player = "Oh... how... I... can't... talk...";

    public const string DateConvo1Player =
        "Oh what a <color=#ff0000ff>{0}</color> I have become... I can't let you <color=#ff0000ff>{1}</color>!";

    public const string DateConvo2Player =
        "You cannot <color=#ff0000ff>{0}</color>, won't you <color=#ff0000ff>{1}</color>?";

    public const string DateConvo3Player =
            "It's a <color=#ff0000ff>{0}</color> that I brought this handy jar of honey then! We could <color=#ff0000ff>{1}</color> and <color=#ff0000ff>{2}</color>!"
        ;

    public const string DateConvo4Player =
            "Oh <color=#ff0000ff>{0}</color>, I shall prepare <color=#ff0000ff>{1}</color> to end this and we can <color=#ff0000ff>{2}</color>"
        ;

    public const string Blank = "<blank>";

    public static List<string> GainedChoices = new List<string> {Blank};

    public static readonly List<string> ChoicesList = new List<string>
    {
        DateConvo1Part1Option1,
        DateConvo1Part1Option2,
        DateConvo1Part1Option3,
        DateConvo1Part2Option1,
        DateConvo1Part2Option2,
        DateConvo1Part2Option3,
        DateConvo2Part1Option1,
        DateConvo2Part1Option2,
        DateConvo2Part1Option3,
//        DateConvo2Part2Option1,
        DateConvo2Part2Option2,
        DateConvo2Part2Option3,
        DateConvo3Part1Option1,
        DateConvo3Part1Option2,
        DateConvo3Part1Option3,
        DateConvo3Part2Option1,
        DateConvo3Part2Option2,
        DateConvo3Part2Option3,
        DateConvo3Part3Option1,
        DateConvo3Part3Option2,
        DateConvo3Part3Option3,
        DateConvo4Part1Option1,
        DateConvo4Part1Option2,
        DateConvo4Part1Option3,
        DateConvo4Part2Option1,
        DateConvo4Part2Option2,
        DateConvo4Part2Option3,
//        DateConvo4Part3Option1,
        DateConvo4Part3Option2,
        DateConvo4Part3Option3
    };

    public const string DateConvo1Part1Option1 = "one Dimensional Princess";
    public const string DateConvo1Part1Option2 = "Well Manicured Explorer";
    public const string DateConvo1Part1Option3 = "smithee of Horse Shoes";

    public static readonly List<string> DateConvo1Choice1 = new List<string>
    {
        Blank,
        DateConvo1Part1Option1,
        DateConvo1Part1Option2,
        DateConvo1Part1Option3
    };

    public const string DateConvo1Part2Option1 = "win without a Fight";
    public const string DateConvo1Part2Option2 = "own a car";
    public const string DateConvo1Part2Option3 = "find an excuse to make this a first person shooter rhythm game";

    public static readonly List<string> DateConvo1Choice2 = new List<string>
    {
        Blank,
        DateConvo1Part2Option1,
        DateConvo1Part2Option2,
        DateConvo1Part2Option3
    };

    public const string DateConvo2Part1Option1 = "simply be as mean as you look";

    public const string DateConvo2Part1Option2 =
        "simply walk into a copyrighted fictional state famous for orcs, rings and volcanic mountains";

    public const string DateConvo2Part1Option3 = "simply create a game in a two day window and expect a BAFTA";

    public static readonly List<string> DateConvo2Choice1 = new List<string>
    {
        Blank,
        DateConvo2Part1Option1,
        DateConvo2Part1Option2,
        DateConvo2Part1Option3
    };

    public const string DateConvo2Part2Option1 = "please let me pass";
    public const string DateConvo2Part2Option2 = "give me an extra chance to choose a better script";
    public const string DateConvo2Part2Option3 = "help me defeat my evil twin sister";

    public static readonly List<string> DateConvo2Choice2 = new List<string>
    {
        Blank,
        DateConvo2Part2Option1,
        DateConvo2Part2Option2,
        DateConvo2Part2Option3
    };

    public const string DateConvo3Part1Option1 = "good job";
    public const string DateConvo3Part1Option2 = "terrible plan";
    public const string DateConvo3Part1Option3 = "sack of Badgers";

    public static readonly List<string> DateConvo3Choice1 = new List<string>
    {
        Blank,
        DateConvo3Part1Option1,
        DateConvo3Part1Option2,
        DateConvo3Part1Option3
    };

    public const string DateConvo3Part2Option1 = "share it together";
    public const string DateConvo3Part2Option2 = "not even think about consuming it";

    public const string DateConvo3Part2Option3 =
        "go join another game jam where the developers have skill and creativity";

    public static readonly List<string> DateConvo3Choice2 = new List<string>
    {
        Blank,
        DateConvo3Part2Option1,
        DateConvo3Part2Option2,
        DateConvo3Part2Option3
    };

    public const string DateConvo3Part3Option1 = "forget about fighting";
    public const string DateConvo3Part3Option2 = "maybe lick the outside of it (out of curiousity)";

    public const string DateConvo3Part3Option3 =
        "try our hardest to raise our power levels through montage and increased gravity training!";

    public static readonly List<string> DateConvo3Choice3 = new List<string>
    {
        Blank,
        DateConvo3Part3Option1,
        DateConvo3Part3Option2,
        DateConvo3Part3Option3
    };

    public const string DateConvo4Part1Option1 = "that is wonderful to hear";
    public const string DateConvo4Part1Option2 = "alright then";
    public const string DateConvo4Part1Option3 = "fiddlesticks";

    public static readonly List<string> DateConvo4Choice1 = new List<string>
    {
        Blank,
        DateConvo4Part1Option1,
        DateConvo4Part1Option2,
        DateConvo4Part1Option3
    };

    public const string DateConvo4Part2Option1 = "an additional button";
    public const string DateConvo4Part2Option2 = "Instant Noodles";
    public const string DateConvo4Part2Option3 = "poor dialogue options";

    public static readonly List<string> DateConvo4Choice2 = new List<string>
    {
        Blank,
        DateConvo4Part2Option1,
        DateConvo4Part2Option2,
        DateConvo4Part2Option3
    };

    public const string DateConvo4Part3Option1 = "ride off into the sunset together";
    public const string DateConvo4Part3Option2 = "go to the dentist";

    public const string DateConvo4Part3Option3 =
            "think about the life decisions we both made to get to this point, subsequently ponder and direct our misgivings of relationship stereotypes, the ongoing work to equalise gender roles in the media and improve the quality of role models in a well constructed debate."
        ;

    public static readonly List<string> DateConvo4Choice3 = new List<string>
    {
        Blank,
        DateConvo4Part3Option1,
        DateConvo4Part3Option2,
        DateConvo4Part3Option3
    };

    public static readonly List<string> CorrectAnswers = new List<string>
    {
        DateConvo1Part1Option1,
        DateConvo1Part2Option1,
        DateConvo2Part1Option1,
        DateConvo2Part2Option1,
        DateConvo3Part1Option1,
        DateConvo3Part2Option1,
        DateConvo3Part3Option2,
        DateConvo4Part1Option1,
        DateConvo4Part2Option1,
        DateConvo4Part3Option1
    };

    public const string DatePreconvo1Boss =
        "I didn't get my Masters in English Literature to let some poorly designed character take ME down!";

    public const string DatePreconvo2Boss =
            "Hmmm... maybe we don't have to fight so hard, I find your prose to be profoundly prophetic and perplexingly pleasurably paralysing..."
        ;

    public const string DateConvo1Boss =
            "Chortle Chortle, your grammar structure is wanting!\n\nBut please, take another shot with your literatic capability!"
        ;

    public const string DateConvo2Boss =
        "Oh how you sway me like a soiled bed sheet on a washing line, but simile cannot let you pass!";

    public const string DateConvo3Boss =
        "Truly I did not wish to be your adverb-sary, I do say that your preposition has moved me.";

    public const string DateConvo4Boss =
            "What strong characterisation you have! I will gladly take a stanza with you against violence and this poorly scripted morality play!"
        ;

    public const string DateConvoBossFail =
            "Sorry, but your grammar and sentance structure are too poor for my tastes.  Your true love is in another castle."
        ;

    public const string DateConvoBossHoney =
        "OH NO! I AM ALLERGIC TO SPELLING B's!\n\nAHHHHH... to Beeeeee... or not to beeeee... eugh.";
}