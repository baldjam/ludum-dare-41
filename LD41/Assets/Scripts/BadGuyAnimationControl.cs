﻿using UnityEngine;

public class BadGuyAnimationControl : MonoBehaviour
{
    float dirX, moveSpeed;
    private Vector3 _prevPosition;

    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        moveSpeed = 5f;
        _prevPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isDead)
            return;
        
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("BGIdle"))
        {
            if (!_prevPosition.Equals(transform.position))
            {
                anim.SetBool("isWalking", true);
            }
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("BGwalking"))
        {
            if (_prevPosition.Equals(transform.position))
            {
                anim.SetBool("isWalking", false);
            }
        }
        
        _prevPosition = transform.position;
    }
    
    public void ShowAttack()
    {
        anim.SetBool("isWalking", false);
        anim.SetTrigger("hit");
    }

    private bool _isDead;

    public void ShowDeath()
    {
        _isDead = true;
        anim.SetBool("isWalking", false);
        anim.SetTrigger("die");
    }
}
