﻿using UnityEngine;

public class PlayerAnimationControl : MonoBehaviour
{
    float dirX, moveSpeed;

    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        moveSpeed = 5f;

    }

    // Update is called once per frame
    void Update()
    {
        dirX = Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime;
        
        if (dirX != 0 && !anim.GetCurrentAnimatorStateInfo(0).IsName("MainChar_attack"))
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }

        if (Input.GetButtonDown("Fire1") && !anim.GetCurrentAnimatorStateInfo(0).IsName("MainChar_attack"))
        {
            anim.SetBool("isWalking", false);
            anim.SetTrigger("hit");
        }
    }
}
