﻿using UnityEngine;
using System.Collections;

public class ParallaxScrolling : MonoBehaviour {
    public Transform cam = null;
    public float xAmount = 1.0f, yAmount = 0.2f;

    private Vector2 newPos;
    private float x0, y0, xDiff, yDiff;

	// Use this for initialization
	void Start () {
        x0 = cam.position.x;
        y0 = cam.position.y;
	}
	
	// Update is called once per frame
	void Update () {
        xDiff = x0 + (cam.position.x * xAmount);
        yDiff = y0 = (cam.position.y * yAmount);
        newPos = new Vector2(cam.position.x - xDiff, cam.position.y - yDiff);
        
        // This = was an -.  I'm guessing this is correct, but I'm not sure what else you've done for it? - Ben
        transform.position = newPos;
	}
}
