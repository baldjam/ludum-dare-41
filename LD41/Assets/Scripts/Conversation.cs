﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Conversation : MonoBehaviour
{
    public Text PlayerText;
    public Text BossText;
    public Dropdown Dropdown1;
    public Dropdown Dropdown2;
    public Dropdown Dropdown3;
    public GameObject ChoicesPanel;
    public Button HoneyButton;
    public Button SunsetButton;
    public Button Continue;

    public Image PlayerSprite;
    public Image BossSprite;

    public Sprite PlayerActive;
    public Sprite PlayerInactive;
    public Sprite BossActive;
    public Sprite BossInactive;

    public AudioSource ClipPlayer;

    public AudioClip BossPositiveClip;
    public AudioClip BossNegativeClip;
    public AudioClip PlayerPositiveClip;
    public AudioClip PlayerNegativeClip;

    private string _playerText;
    private const int ScoreToWin = 8;

    public void PopulateChoices(List<string> choices1, List<string> choices2, List<string> choices3)
    {
        ChoicesPanel.SetActive(true);

        SetDropdownValue(Dropdown1, choices1);
        SetDropdownValue(Dropdown2, choices2);
        SetDropdownValue(Dropdown3, choices3);
    }

    private void SetDropdownValue(Dropdown dropdown, List<string> choices)
    {
        dropdown.options.Clear();

        if (choices != null && choices.Count > 0)
        {
            dropdown.gameObject.SetActive(true);
            dropdown.options.AddRange(choices.Select(x => new Dropdown.OptionData {text = x}));
            dropdown.value = 0;
            dropdown.RefreshShownValue();
        }
        else
        {
            dropdown.gameObject.SetActive(false);
        }
    }

    public void SetNewPlayerDialogue(string newText)
    {
        _playerText = newText;
        BossText.text = string.Empty;
        GeneratePlayerText();
    }

    public void GeneratePlayerText()
    {
        if (Dropdown3.IsActive())
            PlayerText.text = string.Format(_playerText, Dropdown1.captionText.text, Dropdown2.captionText.text, Dropdown3.captionText.text);
        else if (Dropdown2.IsActive())
            PlayerText.text = string.Format(_playerText, Dropdown1.captionText.text, Dropdown2.captionText.text);
        else if (Dropdown1.IsActive())
            PlayerText.text = string.Format(_playerText, Dropdown1.captionText.text);
        else
            PlayerText.text = _playerText;
    }

    public void ShowBossText(string newText)
    {
        ChoicesPanel.SetActive(false);
        PlayerText.text = string.Empty;
        BossText.text = newText;
    }
     
    public void SetNewOption()
    {
        GeneratePlayerText();
    }

    private bool _playerTurn;
    public int Points;

    void Start()
    {
        FindObjectOfType<BackgroundMusic>().AudioSource.volume = 0.25f;
        ProgressDialogue();
    }

    public void ProgressDialogue()
    {
        if (!_playerTurn)
        {
            PlayerSprite.sprite = PlayerActive;
            BossSprite.sprite = BossInactive;
            _playerTurn = true;
            MoveToNextPlayerText();
            return;
        }

        bool playPositive = false;

        if (ChoicesPanel.activeSelf)
        {
            playPositive = true;

            playPositive &= CheckCorrectAnswer(Dropdown1);
            playPositive &= CheckCorrectAnswer(Dropdown2);
            playPositive &= CheckCorrectAnswer(Dropdown3);
        }

        PlayerSprite.sprite = PlayerInactive;
        BossSprite.sprite = BossActive;

        ClipPlayer.Stop();
        ClipPlayer.PlayOneShot(playPositive ? BossPositiveClip : BossNegativeClip);

        _playerTurn = false;
        MoveToNextBossText();
    }

    public void RideIntoSunset()
    {
        SceneManager.LoadScene("WinScreen");
    }

    public void ThrowHoney()
    {
        // play animation of honey being thrown across room
        // change boss sprite

        HoneyButton.gameObject.SetActive(false);
        ProgressDialogue();
    }

    private bool CheckCorrectAnswer(Dropdown dropdown)
    {
        if (dropdown.IsActive())
        {
            if (ConversationText.CorrectAnswers.Contains(dropdown.captionText.text))
            {
                Points += 1;
                return true;
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    private int _playerMessageCounter;
    
    private void MoveToNextPlayerText()
    {
        _playerMessageCounter++;

        ClipPlayer.Stop();

        if (_playerMessageCounter < 3)
        {
            ClipPlayer.PlayOneShot(PlayerNegativeClip);
        }
        else if (_playerMessageCounter < 7)
        {
            ClipPlayer.PlayOneShot(PlayerPositiveClip);
        }
        else
        {
            ClipPlayer.PlayOneShot(Points >= ScoreToWin ? PlayerPositiveClip : PlayerNegativeClip);
        }

        switch (_playerMessageCounter)
        {
            case 1:
                ChoicesPanel.SetActive(false);
                SetNewPlayerDialogue(ConversationText.DatePreconvo1Player);
                break;
            case 2:
                ChoicesPanel.SetActive(false);
                SetNewPlayerDialogue(ConversationText.DatePreconvo2Player);
                break;
            case 3:
                List<string> dateConvo1Choice1 = ConversationText.DateConvo1Choice1.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();
                List<string> dateConvo1Choice2 = ConversationText.DateConvo1Choice2.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();

                PopulateChoices(dateConvo1Choice1, dateConvo1Choice2, null);
                SetNewPlayerDialogue(ConversationText.DateConvo1Player);
                break;
            case 4:
                List<string> dateConvo2Choice1 = ConversationText.DateConvo2Choice1.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();
                List<string> dateConvo2Choice2 = ConversationText.DateConvo2Choice2.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();

                PopulateChoices(dateConvo2Choice1, dateConvo2Choice2, null);
                SetNewPlayerDialogue(ConversationText.DateConvo2Player);
                break;
            case 5:
                List<string> dateConvo3Choice1 = ConversationText.DateConvo3Choice1.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();
                List<string> dateConvo3Choice2 = ConversationText.DateConvo3Choice2.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();
                List<string> dateConvo3Choice3 = ConversationText.DateConvo3Choice3.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();

                PopulateChoices(dateConvo3Choice1, dateConvo3Choice2, dateConvo3Choice3);
                SetNewPlayerDialogue(ConversationText.DateConvo3Player);
                break;
            case 6:
                List<string> dateConvo4Choice1 = ConversationText.DateConvo4Choice1.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();
                List<string> dateConvo4Choice2 = ConversationText.DateConvo4Choice2.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();
                List<string> dateConvo4Choice3 = ConversationText.DateConvo4Choice3.Where(x => ConversationText.GainedChoices.Contains(x)).ToList();

                PopulateChoices(dateConvo4Choice1, dateConvo4Choice2, dateConvo4Choice3);
                SetNewPlayerDialogue(ConversationText.DateConvo4Player);
                break;
            case 7:
                if (Points >= ScoreToWin)
                {
                    SunsetButton.gameObject.SetActive(true);
                }
                else
                {
                    HoneyButton.gameObject.SetActive(true);
                }

                Continue.gameObject.SetActive(false);

                break;
            case 8:
                SceneManager.LoadScene("LoseScreen");
                break;
            default:
                //_playerMessageCounter = 0;
                //DemoPlayerTurn();
                break;
        }
    }

    private int _bossMessageCounter;
    
    private void MoveToNextBossText()
    {
        _bossMessageCounter++;

        switch (_bossMessageCounter)
        {
            case 1:
                ShowBossText(ConversationText.DatePreconvo1Boss);
                break;
            case 2:
                ShowBossText(ConversationText.DatePreconvo2Boss);
                break;
            case 3:
                ShowBossText(ConversationText.DateConvo1Boss);
                break;
            case 4:
                ShowBossText(ConversationText.DateConvo2Boss);
                break;
            case 5:
                ShowBossText(ConversationText.DateConvo3Boss);
                break;
            case 6:
                ShowBossText(Points >= ScoreToWin ? ConversationText.DateConvo4Boss : ConversationText.DateConvoBossFail);
                break;
            case 7:
                ShowBossText(ConversationText.DateConvoBossHoney);
                Continue.gameObject.SetActive(true);
                break;
            default:
                //_bossMessageCounter = 0;
                //DemoBossTurn();
                break;
        }
    }
}
