﻿using System;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class EnemyHealth : MonoBehaviour
{
    public int MaxHealth;
    public GameObject WordPickupPrefab;
    public BadGuyAnimationControl AnimationControl;
    public SpriteRenderer BaddieSprite;
    private int _currentHealth;

    // Use this for initialization
    void Start()
    {
        _currentHealth = MaxHealth;
    }

    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;

        if (_currentHealth <= 0)
        {
            KillEnemy();
        }
    }

    private void KillEnemy()
    {
        BaddieSprite.sortingOrder = 5;
        AnimationControl.ShowDeath();
        GetComponent<Collider2D>().enabled = false;
        GetComponent<EnemyMovement>().Dead = true;
//        gameObject.SetActive(false);

        if (ConversationText.ChoicesList.Count == 0)
        {
            return;
        }

        Random randomGen = new Random();
        string word = ConversationText.ChoicesList.ElementAt(randomGen.Next(0, ConversationText.ChoicesList.Count - 1));

        ConversationText.ChoicesList.Remove(word);

        var newWord = GameObject.Instantiate(WordPickupPrefab, transform.position, transform.rotation);
        newWord.GetComponent<Pickup>().Word = word;
    }
}
