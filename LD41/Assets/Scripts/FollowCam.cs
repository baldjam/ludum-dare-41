﻿using UnityEngine;

public class FollowCam : MonoBehaviour
{
    public Transform Player;

    private float _yPos;
    private float _zPos;

    // Use this for initialization
    void Start()
    {
        _yPos = transform.position.y;
        _zPos = transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        float playerMoveChange = Player.position.x - transform.position.x;
        float newXPos = transform.position.x;

        if (playerMoveChange > 1)
            newXPos = Player.position.x - 1;
        else if (playerMoveChange < -1)
            newXPos = Player.position.x + 1;

        transform.position = new Vector3(newXPos, _yPos, _zPos);
    }
}
