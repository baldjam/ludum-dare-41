﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int MaxHealth;
    public static int CurrentHealth;
    public GameObject HeartPrefab;

    public AudioSource AudioSource;
    public AudioClip DamageFx;
    private GameObject[] _healthBar;

    private readonly Vector3 _offset = new Vector3(50, 0);

    // Use this for initialization
    void Start()
    {
        if (CurrentHealth == 0)
            CurrentHealth = MaxHealth;

        _healthBar = new GameObject[CurrentHealth];

        Transform canvasTransform = FindObjectOfType<HealthGenerator>().transform;
        Vector3 nextPosition = canvasTransform.position;
        Vector3 currentOffset = Vector3.zero;

        for (int i = 0; i < CurrentHealth; i++)
        {
            var heart = Instantiate(HeartPrefab);
            heart.transform.SetParent(canvasTransform, false);
            heart.transform.position = nextPosition + currentOffset;
            currentOffset += _offset;
            _healthBar[i] = heart;
        }
    }

    public void TakeDamage(int damage)
    {
        CurrentHealth -= damage;
        Destroy(_healthBar[CurrentHealth]);
        
        if (CurrentHealth <= 0)
        {
            KillPlayer();
        }

        AudioSource.PlayOneShot(DamageFx);
    }

    private void KillPlayer()
    {
        SceneManager.LoadScene(MenuChooser.GameOverScene);
    }
}
