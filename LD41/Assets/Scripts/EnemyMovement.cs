﻿using System;
using System.Threading;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private GameObject _player;

    private int _speed = 2;
    private bool _canAttack = true;
    public bool Dead { get; set; }
    public BadGuyAnimationControl AnimationControl;

    // Use this for initialization
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Dead)
            return;

        CheckForDeath();
        AttemptMove();
        AttemptAttack();
    }

    private void CheckForDeath()
    {
        Vector3 positionOffset = new Vector3(-0.1f, 0);
        Vector3 newPosition = transform.position + positionOffset;

        var hits = Physics2D.LinecastAll(transform.position, newPosition);

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.CompareTag("Attack"))
            {
                GetComponent<EnemyHealth>().TakeDamage(1);
                Destroy(hit.collider.gameObject);
                return;
            }
        }
    }

    private void AttemptAttack()
    {
        if (!_canAttack)
        {
            return;
        }

        Vector3 positionOffset = new Vector3(-1f, 0);
        Vector3 newPosition = transform.position + positionOffset;

        var hits = Physics2D.LinecastAll(transform.position, newPosition);

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.CompareTag("Player"))
            {
                AnimationControl.ShowAttack();
                var health = hit.collider.GetComponent<PlayerHealth>();
                health.TakeDamage(1);
                _canAttack = false;
                _canMove = false;
                // ReSharper disable once ObjectCreationAsStatement
                new Timer((state => _canAttack = true), null, new TimeSpan(0, 0, 1), new TimeSpan(1,0,0));
                return;
            }
        }

        _canMove = true;
    }

    private bool _canMove = true;

    private void AttemptMove()
    {
        float xMove = 0;

        if (Vector3.Distance(transform.position, _player.transform.position) > 8f)
        {
            return;
        }

        if (_player.transform.position.x < transform.position.x)
            xMove = _speed * -1 * Time.deltaTime;
        else if (_player.transform.position.x > transform.position.x)
            xMove = _speed * Time.deltaTime;

        float yMove = 0;

        if (_player.transform.position.y < transform.position.y)
            yMove = _speed * -1 * Time.deltaTime;
        else if (_player.transform.position.y > transform.position.y)
            yMove = _speed * Time.deltaTime;

        Vector3 positionOffset = new Vector3(xMove, yMove);

        Vector3 newPosition = transform.position + positionOffset;

        var hits = Physics2D.LinecastAll(transform.position, newPosition);

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.CompareTag("Player"))
            {
                _canMove = false;
                break;
            }
        }

        if (_canMove)
        {
            transform.position += positionOffset;
        }
    }
}
