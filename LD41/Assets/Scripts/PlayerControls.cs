﻿using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    public const float HorizontalWalkSpeed = 6f;
    public const float VerticalWalkSpeed = 3f;
    private bool _hasAttacked;

    public GameObject AttackPrefab;
    public AudioSource SoundFxPlayer;
    public AudioClip AttackNoise;

    // Use this for initialization

    // Update is called once per frame
    void Update()
    {

        AttemptMove();
        AttemptAttack();
        FreeUpAttack();
    }

    private void FreeUpAttack()
    {
        if (Input.GetAxis("Fire1") <= 0)
        {
            _hasAttacked = false;
        }
    }

    private void AttemptAttack()
    {
        if (_hasAttacked || Input.GetAxis("Fire1") <= 0)
        {
            return;
        }

        _hasAttacked = true;

        SoundFxPlayer.PlayOneShot(AttackNoise);
        Instantiate(AttackPrefab, transform.position, transform.rotation);
    }

    private void AttemptMove()
    {
        float xMove = 0f, yMove = 0f;

        if (Input.GetAxis("Horizontal") != 0)
        {
            xMove = Input.GetAxis("Horizontal") * Time.deltaTime * HorizontalWalkSpeed;
        }

        if (Input.GetAxis("Vertical") != 0)
        {
            yMove = Input.GetAxis("Vertical") * Time.deltaTime * VerticalWalkSpeed;
        }

        Vector3 positionOffset = new Vector3(xMove, yMove);

        Vector3 newPosition = transform.position + positionOffset;

        bool canMove = true;

        var hits = Physics2D.LinecastAll(transform.position, newPosition);

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.CompareTag("Blocker") || hit.collider.CompareTag("Enemy"))
            {
                canMove = false;
            }
            else if (hit.collider.CompareTag("Pickup"))
            {
                string gainedWord = hit.collider.GetComponent<Pickup>().Word;

                if (!ConversationText.GainedChoices.Contains(gainedWord))
                {
                    ConversationText.GainedChoices.Add(gainedWord);
                }

                Destroy(hit.collider.gameObject);
            }
            else if (hit.collider.CompareTag("SceneTransition"))
            {
                hit.collider.GetComponent<RoomTransition>().MoveRoom();
            }
        }

        if (canMove)
        {
            transform.position += positionOffset;
        }
    }
}