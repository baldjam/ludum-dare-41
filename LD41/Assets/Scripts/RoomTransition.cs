﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomTransition : MonoBehaviour
{
    public string NextRoom;

    public void MoveRoom()
    {
        SceneManager.LoadScene(NextRoom);
    }
}
