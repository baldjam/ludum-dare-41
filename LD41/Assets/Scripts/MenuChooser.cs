﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuChooser : MonoBehaviour
{
    public const string TitleScene = "Title";
    public const string GameScene = "Game";
    public const string WinScene = "WinScreen";
    public const string GameOverScene = "LoseScreen";
    public const string Level1 = "1-1_Start";

    public void LoadTitle()
    {
        SceneManager.LoadScene(TitleScene);
    }

    public void LoadGame()
    {
        SceneManager.LoadScene(Level1);
    }

    public void LoadWin()
    {
        SceneManager.LoadScene(WinScene);
    }

    public void LoadGameOver()
    {
        SceneManager.LoadScene(GameOverScene);
    }
}
