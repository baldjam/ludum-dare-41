﻿using UnityEngine;

public class LoseScreen : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        var musicPlayer = FindObjectOfType<BackgroundMusic>();

        if (musicPlayer != null)
        {
            musicPlayer.AudioSource.volume = 0.5f;
        }

        PlayerHealth.CurrentHealth = 0;
    }
}
