﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationHelper : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    AddOption(ConversationText.DateConvo1Part1Option1);
	    AddOption(ConversationText.DateConvo1Part2Option3);
	    AddOption(ConversationText.DateConvo2Part1Option2);
	    AddOption(ConversationText.DateConvo2Part2Option2);
	    AddOption(ConversationText.DateConvo3Part1Option3);
	    AddOption(ConversationText.DateConvo3Part2Option3);
	    AddOption(ConversationText.DateConvo3Part3Option1);
	    AddOption(ConversationText.DateConvo4Part1Option1);
	    AddOption(ConversationText.DateConvo4Part2Option1);
	    AddOption(ConversationText.DateConvo4Part3Option3);
	}

    private static void AddOption(string choice)
    {
        if (!ConversationText.GainedChoices.Contains(choice))
        {
            ConversationText.GainedChoices.Add(choice);
        }
        ConversationText.ChoicesList.Remove(choice);
    }

    // Update is called once per frame
	void Update () {
		
	}
}
