﻿using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    public AudioSource AudioSource;

    void Awake()
    {
        var musicPlayers = FindObjectsOfType<BackgroundMusic>();

        foreach (var backgroundMusic in musicPlayers)
        {
            if (this != backgroundMusic)
            {
                Destroy(backgroundMusic.gameObject);
                return;
            }
        }

        DontDestroyOnLoad(transform.gameObject);
    }

    void Start()
    {
        PlayMusic();
    }

    public void PlayMusic()
    {
        if (AudioSource.isPlaying)
        {
            return;
        }

        AudioSource.Play();
    }

    public void StopMusic()
    {
        AudioSource.Stop();
    }
}
